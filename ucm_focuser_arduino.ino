
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61); 

// Connect a stepper motor with 200 steps per revolution (1.8 degree)
// to motor port #2 (M3 and M4)
Adafruit_StepperMotor *myMotor = AFMS.getStepper(200, 2);

void setup()
{
 Serial.begin(9600);
 pinMode(13, OUTPUT);

  AFMS.begin();  // create with the default frequency 1.6KHz
  //AFMS.begin(1000);  // OR with a different frequency, say 1KHz
  
  myMotor->setSpeed(20);
 
}
void loop()
{
 if (Serial.available() > 0)
 {
  int b = Serial.read();
  if (b == 4)
  {
      step_forward(20); 
  }
    if (b == 3)
  {
      step_forward(1); 
  }
    if (b == 2)
  {
      step_backward(1); 
  }
    if (b == 1)
  {
      step_backward(20);   
  }
  Serial.flush();
 }
}

void step_forward(int x){
  myMotor->step(x, FORWARD, MICROSTEP); 
  myMotor->release();
}

void step_backward(int x){
  myMotor->step(x, BACKWARD, MICROSTEP);
  myMotor->release();
}

